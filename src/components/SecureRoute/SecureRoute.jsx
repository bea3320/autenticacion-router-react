import React from 'react';
import {Redirect, Route} from 'react-router-dom';

const SecureRoute = (props) => {

    if (props.hasUser === null) {
        //Aún no ha vuelto la petición
        return(<div>Cargando....</div>);
    }
    if(props.hasUser) {
        //tenemos usuario: true
        return(<Route {...props} />)
    }
    if(!props.hasUser) {
        //no hay usuario logueado
        return (<Redirect to="/login" />)
    }
};

export default SecureRoute;