import { useState } from "react";
import { register } from "../../api/auth";
import './RegisterForm.scss';
//1. crear un estado vacío con los campos que contendrá mi formulario
//2. crear el formulario html
//3. crear función para el inputChange
//4. crear función para enviar al formulario
//5. Llamar a la función que hace el fecth a nuestro servidor

const INITIAL_STATE = {
  //vemos que valores son los que están definidos en el archivo de node
  username: "",
  email: "",
  password: "",
};

const RegisterForm = (props) => {
  //creamos las variables de estado
  const [formFields, setFormFields] = useState(INITIAL_STATE);
  const [error, setError] = useState(null);

  const handleFormSubmit = async (event) => {
    //1. Recopilar los datos (formFields)
    //2. Enviar Petición al back de autenticación
    event.preventDefault();
    /* console.log('FORMULARIO PARA ENVIAR', formFields); */
    try{
      const user = await register(formFields);
      props.saveUser(user);
      setError(null);
      /* console.log('REGISTER USER', user); */
      setFormFields(INITIAL_STATE);
    }catch(error){
      setError(error.message);
    }
    
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormFields({ ...formFields, [name]: value });
    console.log(formFields);
  };
  return (
    <div className="register-form">
      <h3>Registro</h3>
      <form onSubmit={handleFormSubmit}>
        <label htmlFor="username">
          <p>Nombre de usuario</p>
          <input
            type="text"
            name="username"
            id="username"
            placeholder="Nombre de Usuario"
            onChange={handleInputChange}
            value={formFields.username}
          />
        </label>
        <label htmlFor="email">
          <p>Email</p>
          <input
            type="email"
            name="email"
            id="email"
            placeholder="Email"
            onChange={handleInputChange}
            value={formFields.email}
          />
        </label>
        <label htmlFor="password">
          <p>Contraseña</p>
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Contraseña"
            onChange={handleInputChange}
            value={formFields.password}
          />
        </label>
        <div className="register-form__button">
          <button type="submit">Registrarme</button>
        </div>
      </form>
      {error && <div className="register-form__error">
        {error}
        </div>}
    </div>
  );
};

export default RegisterForm;
