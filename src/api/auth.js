//register = https://upgrade-auth.herokuapp.com/api/secret/auth/register   esta es la url a la que haremos la petición

const registerUrl = "http://localhost:4000/auth/register";
const loginUrl = "http://localhost:4000/auth/login";
const checkSessionUrl= "http://localhost:4000/auth/check-session";
const logoutUrl = "http://localhost:4000/auth/logout";

export const register = async (userData) => {
    const request = await fetch(registerUrl, {
        method:'POST',
        headers: {
            'Accept': 'applicaton/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        credentials: 'include',
        body: JSON.stringify(userData)
    });
    /* console.log(await response.json()); */
    const response = await request.json()

    if(!request.ok) {
        throw new Error(response.message)
    }
    return response;
};

export const login = async (userData) => {
    const request = await fetch(loginUrl, {
        method:'POST',
        headers: {
            'Accept': 'applicaton/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        credentials: 'include',
        body: JSON.stringify(userData)
    });
    /* console.log(await response.json()); */
    const response = await request.json()

    if(!request.ok) {
        throw new Error(response.message)
    }
    return response;
};

export const checkSession = async () => {
    const request = await fetch(checkSessionUrl, {
        method:'GET',
        headers: {
            'Accept': 'applicaton/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        credentials: 'include',
    });

    const response = await request.json();
    /* console.log('RESPONSE CHECK SESSION',  response); */
    return response; 
};

export const logout = async () => {
    const request = await fetch(logoutUrl, {
        method:'POST',
        headers: {
            'Accept': 'applicaton/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*',
        },
        credentials: 'include',
    });

    const response = await request.json();
    
    return response;
}

