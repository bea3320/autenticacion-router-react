import { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { checkSession } from "./api/auth";
import {
  RegisterForm,
  LoginForm,
  Navbar,
  Home,
  SecureRoute,
  ComponentePrueba,
} from "./components";
import "./App.scss";

const App = () => {
  const [user, setUser] = useState(null);
  const [hasUser, setHasUser] = useState(null);

  useEffect(() => {
    if (user) {
      console.log("CONFIRMAMOS QUE TENEMOS USUARIO");
    } else {
      getUser();
      /* console.log(user); */
    }
  }, [user]);

  const getUser = async () => {
    try {
      const user = await checkSession();
      if (!user.message) {
        setUser(user);
        setHasUser(true);
      } else {
        setHasUser(false);
      }
    } catch (error) {
      console.log("error", error);
      setHasUser(false);
    }
  };

  const saveUser = (user) => {
    /* console.log('GUARDAR USUARIO: ', user); */
    setUser(user);
    setHasUser(true);
  };

  const deleteUser = () => {
    setUser(null);
    hasUser(false);
  };

  return (
    <Router>
      <div className="app">
      <Navbar user={user} deleteUser={deleteUser} />
        <h1>REACT</h1>
        <h2>AUTENTICACIÓN && ROUTER</h2>
        
        <Switch>
          <Route
            exact
            path="/register"
            component={(props) => (
              <RegisterForm saveUser={saveUser} {...props} />
            )}
          />
          <Route
            exact
            path="/login"
            component={(props) => <LoginForm saveUser={saveUser} {...props} />}
          />
          <SecureRoute
            exact
            path="/componentePrueba"
            hasUser={hasUser}
            component={(props) => <ComponentePrueba {...props} />}
          />
          <Route exact path="/" component={Home} />
        </Switch>
      </div>
    </Router>
  );
};
export default App;
